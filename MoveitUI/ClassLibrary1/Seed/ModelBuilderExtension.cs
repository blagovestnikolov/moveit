﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1.Seed
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder builder)
        {
            var roles = new List<Role>()
            {
                new Role
                {
                    Id = "userid",
                    Name = "User",
                    NormalizedName = "USER"
                }
            };
            builder.Entity<Role>().HasData(roles);
        }
    }
}