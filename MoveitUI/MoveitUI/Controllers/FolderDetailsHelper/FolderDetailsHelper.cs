﻿using Microsoft.Extensions.Configuration;
using MoveitUI.Controllers.TokenHandler;
using MoveitUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MoveitUI.Controllers
{
    public class FolderDetailsHelper : IFolderDetailsHelper
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITokenHelper _tokenhelper;
        private readonly IConfiguration _configuration;

        public FolderDetailsHelper(IHttpClientFactory httpClientFactory, ITokenHelper tokenhelper, IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _tokenhelper = tokenhelper;
            _configuration = configuration;
        }

        public Foler Folder { get; set; }

        private async Task GetFolder()
        {
            var token = _tokenhelper.Token;

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.BaseAddress = new Uri(_configuration.GetValue<string>("BaseAdr"));

            var response = await client.GetAsync("/api/v1/folders");

            if (response.IsSuccessStatusCode)
            {
                var resp = await response.Content.ReadAsAsync<Foler>();
                Folder = resp;
            }
        }

        public async Task GetNewFolderValues()
        {
            await GetFolder();
        }

        public async Task GetNewFolder()
        {
            if (Folder == null)
                await GetFolder();
        }

    }
}
