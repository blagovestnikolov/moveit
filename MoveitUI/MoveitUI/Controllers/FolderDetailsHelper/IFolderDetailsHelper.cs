﻿using MoveitUI.Models;
using System.Threading.Tasks;

namespace MoveitUI.Controllers
{
    public interface IFolderDetailsHelper
    {
        Foler Folder { get; set; }

        Task GetNewFolder();
        Task GetNewFolderValues();
    }
}