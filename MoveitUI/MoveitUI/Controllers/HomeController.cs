﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClassLibrary1;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MoveitUI.Models;

namespace MoveitUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly MoveItContext _context;

        public HomeController(UserManager<User> userManager,
            SignInManager<User> signInManager,
            MoveItContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Register()
        {
            var model = new RegisterModel();
            return View("Register", model);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            var passHasher = new PasswordHasher<User>();
            if (ModelState.IsValid)
            {
                var user = new User() { };
                user.Id = Guid.NewGuid().ToString();
                user.UserName = model.UserName;
                user.PasswordHash = passHasher.HashPassword(user, model.Password);
                user.SecurityStamp = Guid.NewGuid().ToString();

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    var regularUserRole = new IdentityUserRole<string>();
                    regularUserRole.RoleId = "userid";
                    regularUserRole.UserId = user.Id;
                    _context.Add(regularUserRole);
                    _context.SaveChanges();
                    return RedirectToAction("Success", "Home");
                }
            }
            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(new IndexModel());
        }

        [HttpPost]
        public async Task<IActionResult> Index(IndexModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, isPersistent: false, lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "HomeSignIn");
                }
            }
            ViewData["wrongacc"] = "Your username/password is incorrect!";

            return View();
        }

       
        public IActionResult Error()
        {
            return View();
        }
        public IActionResult Success()
        {
            return View();
        }
    }
}