﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using ClassLibrary1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MoveitUI.Controllers.TokenHandler;
using MoveitUI.Models;

namespace MoveitUI.Controllers
{
    [Authorize(Roles = "User")]
    public class HomeSignInController : Controller
    {
        private readonly ILogger<HomeSignInController> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITokenHelper _tokenhelper;
        private readonly IFolderDetailsHelper _folderDetailsHelper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;

        public HomeSignInController(ILogger<HomeSignInController> logger,
            IHttpClientFactory httpClientFactory,
            ITokenHelper tokenhelper,
            IFolderDetailsHelper folderDetailsHelper,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IConfiguration configuration)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _tokenhelper = tokenhelper;
            _folderDetailsHelper = folderDetailsHelper;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> Index(IFormFile file)
        {
            _tokenhelper.GetNewToken();
            await _folderDetailsHelper.GetNewFolder();

            var token = _tokenhelper.Token;
            var b = _folderDetailsHelper.Folder;
            var idFolder = b.Items.FirstOrDefault(f => f.Name == "interview.blagovest.nikolov").Id;

            var fileContent = new StreamContent(file.OpenReadStream())
            {
                Headers =
            {
                ContentLength = file.Length,
                ContentType = new MediaTypeHeaderValue(file.ContentType)
            }
            };

            var formDataContent = new MultipartFormDataContent();
            formDataContent.Add(fileContent, "File", file.FileName);          // file

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.BaseAddress = new Uri(_configuration.GetValue<string>("BaseAdr"));

            var responce = await client.PostAsync($"/api/v1/folders/{idFolder}/files", formDataContent);

            if (responce.IsSuccessStatusCode)
            {
                ViewData["conflict"] = "The File was uploaded succesfully!";
                return View();
            }
            else
            {
                
                if (responce.StatusCode.ToString()== "Conflict")
                {
                    ViewData["conflict"] = "The File already exists!";
                    return View();
                }

                await _folderDetailsHelper.GetNewFolderValues();

                return BadRequest();
            }
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
