﻿using System.Threading.Tasks;

namespace MoveitUI.Controllers.TokenHandler
{
    public interface ITokenHelper
    {
        string Token { get; set; }

        void GetNewToken();

    }
}