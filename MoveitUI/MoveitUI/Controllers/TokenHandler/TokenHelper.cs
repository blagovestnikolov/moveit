﻿using MoveitUI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text;
using Newtonsoft.Json;
using Hangfire;
using Microsoft.Extensions.Configuration;

namespace MoveitUI.Controllers.TokenHandler
{

    public class TokenHelper : ITokenHelper
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;


        public TokenHelper(IHttpClientFactory httpClientFactory, IConfiguration configuration)
        {
            _httpClientFactory = httpClientFactory;
            _configuration = configuration;
           
        }

        public string Token { get; set; }
        private int Exp { get; set; }
        private string RefreshToken { get; set; }

        private void GetToken()
        {

            var user = _configuration.GetSection("UserNa").Value;
            var pass = _configuration.GetValue<string>("PassW");
            var address = _configuration.GetValue<string>("BaseAdr");

            var requestContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("password", pass),
                new KeyValuePair<string, string>("username", user),
                new KeyValuePair<string, string>("grant_type", "password"),

            });
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(address);
            var responce = client.PostAsync("/api/v1/token", requestContent).Result;

            var aaa = 1;

            if (responce.IsSuccessStatusCode)
            {
                var resp = responce.Content.ReadAsAsync<TokenModel>().Result;
                Token = resp.AccesToken;
                Exp = int.Parse(resp.Exp);
                RefreshToken = resp.RefreshToken;
            }

            BackgroundJob.Schedule(() => GetRefreshToken(RefreshToken), TimeSpan.FromSeconds(Exp-60));
        }


        public void GetRefreshToken(string RefreshToken)
        {
            var requestContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("refresh_token", RefreshToken),
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
            });
            requestContent.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri(_configuration.GetValue<string>("BaseAdr"));

            var response = client.PostAsync("/api/v1/token", requestContent).Result;

            if (response.IsSuccessStatusCode)
            {
                var resp = response.Content.ReadAsAsync<TokenModel>().Result;
                Token = resp.AccesToken;
                Exp = int.Parse(resp.Exp);
                RefreshToken = resp.RefreshToken;
            }

            BackgroundJob.Schedule(() => GetRefreshToken(RefreshToken), TimeSpan.FromSeconds(Exp - 60));

        }

        public void GetNewToken()
        {
            if (Token == null)
                GetToken();
        }

    }
}
