﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoveitUI.Middleware
{
    public class ExceptionHandlingMIddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMIddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next.Invoke(httpContext);
            }
            catch(Exception e)
            {

                switch (e.GetType().Name.ToString())
                {
                    case "Object reference not set to an instance of an object.":
                        httpContext.Response.Redirect("/Home/Error");
                        break;

                    default:
                        httpContext.Response.Redirect("/Home/Error");
                        break;
                }
            }

        }

    }

}
