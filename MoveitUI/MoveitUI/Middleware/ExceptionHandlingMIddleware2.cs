﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoveitUI.Middleware
{
    public class ExceptionHandlingMIddleware2
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMIddleware2(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            await _next.Invoke(httpContext);

            switch (httpContext.Response.StatusCode)
            {
                //conflict file add


                case 400:
                    httpContext.Response.Redirect("/Home/Error");
                    break;
                case 401:
                    httpContext.Response.Redirect("/Home/Error");
                    break;
                case 404:
                    httpContext.Response.Redirect("/Home/Error");
                    break;

                case 500:
                    httpContext.Response.Redirect("/Home/Error");
                    break;
            }
        }

    }
}
