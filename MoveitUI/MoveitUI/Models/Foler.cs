﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MoveitUI.Models
{
    [DataContract]
    public class Foler
    {
        [DataMember(Name = "items")]
        public List<Item> Items { get; set; } = new List<Item>();

        [DataMember(Name = "permission")]
        public Permission Permission { get; set; }
    }
}
