﻿using System.Runtime.Serialization;

namespace MoveitUI.Models
{
    [DataContract]
    public class Item
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

    }
}