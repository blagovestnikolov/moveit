﻿using System.Runtime.Serialization;

namespace MoveitUI.Models
{
    [DataContract]
    public class Permission
    {
        [DataMember(Name = "canWriteFiles")]
        public bool CanWrite { get; set; }

    }
}