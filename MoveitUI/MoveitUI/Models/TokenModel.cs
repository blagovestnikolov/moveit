﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MoveitUI.Models
{
    [DataContract]
    public class TokenModel
    {
            
        [DataMember(Name = "access_token")]
        public string AccesToken { get; set; }

        [DataMember(Name = "expires_in")]
        public string Exp { get; set; }

        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }

    }
}
