using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MoveitUI.Controllers;
using MoveitUI.Controllers.TokenHandler;
using Hangfire;
using MoveitUI.Middleware;
using Microsoft.EntityFrameworkCore;
using ClassLibrary1;
using Microsoft.Extensions.Logging;
using Hangfire.MemoryStorage;

namespace MoveitUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MoveItContext>(options =>
            options.UseSqlServer(this.Configuration.GetConnectionString("DefaultConnection")));

            services
               .AddIdentity<User, Role>(options =>
               {
                   options.SignIn.RequireConfirmedAccount = false;
                   options.Password.RequireNonAlphanumeric = false;
                   options.Password.RequireUppercase = false;
                   options.Password.RequiredLength = 3;
                   options.Password.RequireLowercase = false;
                   options.Lockout.AllowedForNewUsers = false;
                   options.Password.RequireDigit = false;

               })
               .AddEntityFrameworkStores<MoveItContext>();

            services.AddControllersWithViews();
            services.AddHttpClient();
            services.AddSingleton<ITokenHelper, TokenHelper>();
            services.AddSingleton<IFolderDetailsHelper, FolderDetailsHelper>();

            services.AddControllers().AddNewtonsoftJson(options =>
                       options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddHangfire(c => c.UseMemoryStorage());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
         {

           
            app.UseMiddleware<ExceptionHandlingMIddleware>();
            app.UseMiddleware<ExceptionHandlingMIddleware2>();

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseMiddleware<ExceptionHandlingMIddleware2>();

            app.UseAuthentication();
            app.UseAuthorization();

            

            app.UseHangfireDashboard();
            app.UseHangfireServer();




            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
