Gitlab repo: https://gitlab.com/blagovestnikolov/moveit

  

## Table of contents

* [Technologies](#technologies)

* [General info](#general-info)

* [Screenshots](#screenshots)

  <br>
  
  

**Technologies**:


**X.PagedList.MVC.Core** (7.6.0)

**Microsoft.EntityFrameworkCore.Tools** (3.1.9)

**Microsoft.EntityFrameworkCore.SqlServer** (3.1.9)

**Microsoft.EntityFrameworkCore.Relational** (3.1.9)

**Microsoft.EntityFrameworkCore.Design** (3.1.9)

**Microsoft.EntityFrameworkCore** (3.1.9)

**Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation** (3.1.13)

**Microsoft.AspNetCore.Mvc.NewtonsoftJson** (3.1.9)

**Microsoft.AspNetCore.Identity.EntityFrameworkCore** (3.1.9)

**Hangfire.AspNetCore** (1.7.23)

**bootstrap** (5.0.1)



  <br/>
  
<br/>
<br/>

## Screenshots

<br>

## Homepage:

User can sign in or register

Note:
If the username/password is incorect the user will be notified.

![](pictures/homep1.png)![](/.png)

<br>

## Register:
User can register passing their username and password.

Requirements:
Username/Password should be between 3 and 20 characters.


![](pictures/Register1.png)

<br>

## Homepage SignIn:
Users can Upload files using the UI.

Note:
If the uploaded file exist in the system the user will be notified.

![](pictures/HomeSign.png)

![](pictures/SuccessUpload.png)
<br>



<br>



## General info

## Solution includes N projects:


* 2.[MVC] > MoveItUI > MVC

* 4.[Data] > ClassLibrary1 > Data

